###############################################################################
# define global variable
## folder which contains .fastq data files and sample description .dat files
FOLDER_FASTQ=$1
## absolute path to the reference database
REF_DATABASE=$2
## prefix of the file name into reference database
base_pref=`ls "$REF_DATABASE"/*sdx | sed 's/_[0-9][0-9][0-9].sdx//'g | awk -F/ '{print $NF}' | uniq`
## number of cores available
CORES=$3
###############################################################################
## list fastq files
for i in `ls "$FOLDER_FASTQ"/*_R1.fastq.gz`;
do
echo $i | cut -d "." -f 1 | sed 's/_R1//g'
done > liste_fq
##list sample description files .dat
for i in `ls "$FOLDER_FASTQ"/*dat`;
do
echo $i
done > liste_dat
## table of fastq and corresponding dat files
paste liste_fq liste_dat > liste_fq_dat
rm liste_fq liste_dat
## writing script bash : commands to apply on fastq/dat files
while IFS= read -r var
do
echo "bash scripts/filter_and_demultiplex.sh "$var" "$CORES
done < liste_fq_dat > fq_dat_cmd.sh
## run in parallel all commands
parallel < fq_dat_cmd.sh

###############################################################################
## concatenate all the sample fas into a one OBI-fasta file
ALL_FAS="main/all.fasta"
python2 scripts/fas_to_obifasta.py -o $ALL_FAS
## ###dereplicate reads into uniq sequences
dereplicated_all="${ALL_FAS/.fasta/.uniq.fasta}"
obiuniq -m sample $ALL_FAS > $dereplicated_all


###############################################################################
#REF_DATABASE="/media/superdisk/edna/donnees/reference_database_teleo/"
#base_pref="embl_std"

##Assign each sequence to a taxon
all_sample_sequences_tag="${dereplicated_all/.fasta/.tag.fasta}"
ecotag -d "$REF_DATABASE"/"${base_pref}" -R "$REF_DATABASE"/db_"${base_pref}".fasta $dereplicated_all > $all_sample_sequences_tag
##The sequences can be sorted by decreasing order of count
all_sample_sequences_sort="${all_sample_sequences_tag/.fasta/.sort.fasta}"
obisort -k count -r $all_sample_sequences_tag > $all_sample_sequences_sort
##generate a table final results
obitab -o $all_sample_sequences_sort > final/all.csv

## Assign each sequence to a taxon according to alternative SPYGEN fasta sequences

spygen_bdr_fasta="/media/superdisk/edna/donnees/rhone_all/teleo_V1_0_VM.fasta"
all_sample_sequences_tag_spygen="${dereplicated_all/.fasta/.tag_spygen.fasta}"
ecotag -d "$REF_DATABASE"/"${base_pref}" -R "$spygen_bdr_fasta" $dereplicated_all > $all_sample_sequences_tag_spygen
##The sequences can be sorted by decreasing order of count
all_sample_sequences_sort="${all_sample_sequences_tag_spygen/.fasta/.sort.fasta}"
obisort -k count -r $all_sample_sequences_tag_spygen > $all_sample_sequences_sort
##generate a table final results
obitab -o $all_sample_sequences_sort > final/all_spygen.csv

