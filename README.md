bash_vsearch_ecotag 
===================

# Table of contents

1. [Introduction](#1-introduction)
2. [Installation](#2-installation)
	1. [Requirements](#21-requirements)
	2. [Initialisation](#22-initialisation)
3. [Reporting bugs](#3-reporting-bugs)
4. [Running the pipeline](#4-running-the-pipeline)

-----------------

# 1. Introduction

Pipeline for edna paired end data to do :
* Assembly, demultiplexing, dereplicate with vsearch/cutadapt
* taxonomy assignation with ECOTAG


To download the project
```
git clone https://gitlab.mbb.univ-montp2.fr/edna/bash_vsearch_ecotag.git
```

To run the whole pipeline into a single command
```
NCORES=16
nohup bash main.sh path/to/fastqdat path/to/refdatabase $NCORES &
```

* path/to/fastqdat : absolute path to a folder with fastq files and .dat information
* path/to/refdatabase : absolute path to the folder of reference database
* $NCORES : number of available cores for this job

Example of command
```
PATH_FASTQC="/media/superdisk/edna/donnees/rhone_test/"
PATH_REFDAT="/media/superdisk/edna/donnees/reference_database_teleo/"
NCORES=16
nohup bash main.sh $PATH_FASTQC $PATH_REFDAT $NCORES &
```
