#===============================================================================
#INFORMATIONS
#===============================================================================
"""
CEFE - EPHE - YERSIN 2018
guerin pierre-edouard
creer un fichier OBIFASTA a partir des sequences SPY.fas generes par VSEARCH
"""
#===============================================================================
#MODULES
#===============================================================================
import argparse
import os
import Bio
from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


#===============================================================================
#ARGUMENTS
#===============================================================================

parser = argparse.ArgumentParser('convert fasta to obiconvert format')
parser.add_argument("-o","--output",type=str)
#parser.add_argument("-f","--ena_fasta",type=str)


#===============================================================================
#MAIN
#===============================================================================

outputFile="all.fasta"

args = parser.parse_args()
outputFile = args.output

mes_records=[]
for fasFile in os.listdir("main/"):
    sampleName, fasExt = os.path.splitext(fasFile)
    if fasExt == ".fas":
    	fasPath="main/"+fasFile
    	print fasFile
    	for seq_record in SeqIO.parse(fasPath, "fasta",alphabet=IUPAC.unambiguous_dna):
        	seq_record_idSplit=seq_record.id.split(";")
        	countFas=seq_record_idSplit[1].split("=")[1]
        	IDFas=seq_record_idSplit[0]
        	local_id=IDFas+" count="+countFas+"; merged_sample={'"+sampleName+"': "+countFas+"};"
        	local_seq=str(repr(seq_record.seq.lower().tostring())).replace("'","")
        	local_record=SeqRecord(Seq(local_seq,IUPAC.unambiguous_dna), id=local_id,description="")
        	mes_records.append(local_record)

SeqIO.write(mes_records, outputFile, "fasta")
